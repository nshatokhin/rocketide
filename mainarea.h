#ifndef MAINAREA_H
#define MAINAREA_H

#include <QMainWindow>

namespace Ui {
class MainArea;
}

class MainArea : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainArea(QWidget *parent = 0);
    ~MainArea();

public slots:
    void createWindow();

private:
    Ui::MainArea *ui;

    bool connectActions();
    bool disconnectActions();
};

#endif // MAINAREA_H
