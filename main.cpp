#include "mainarea.h"
#include <QApplication>

#include <QDebug>

#include "core/engine.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    // 2068427.1
    Engine engine(20, new FuelPair(FuelPair::FUEL_GASOLINE, FuelPair::OXYDIZER_OXYGEN, 300));

    MainArea w;
    w.show();

    return a.exec();
}
