#ifndef NOZZLE_H
#define NOZZLE_H

#include <QObject>

class Engine;
class FuelPair;


class Nozzle : public QObject
{
    Q_OBJECT
public:
    explicit Nozzle(FuelPair *fuelPair, const double &mixtureConsumption, QObject *parent = 0);

    double throatArea() const;
    double exitArea() const;

    double throatRadius() const;
    double exitRadius() const;

    double convergenceAngle() const;
    double divergenceAngle() const;

signals:

public slots:

protected:
    Engine *m_engine;
    FuelPair *m_fuelPair;

    double m_temperatureAtNozzleThroat;
    double m_pressureAtNozzleThroat;

    double m_nozzleThroatArea;
    double m_nozzleExitArea;

    double m_throatRadius;
    double m_exitRadius;

    double m_gasSquareMachNumber;
    double m_gasMachNumber;

    double m_convergenceAngle;
    double m_divergenceAngle;

};

#endif // NOZZLE_H
