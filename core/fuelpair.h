#ifndef FUELPAIR_H
#define FUELPAIR_H

#include <QObject>

class FuelPair : public QObject
{
    Q_OBJECT
public:
    enum Fuel
    {
        FUEL_GASOLINE
    };

    enum Oxydizer
    {
        OXYDIZER_OXYGEN
    };

    explicit FuelPair(const Fuel &fuel, const Oxydizer &oxydizer, const double &pressure, QObject *parent = 0);
    FuelPair(const FuelPair &obj);

    double pressure() const;

    double mixtureRatio() const;
    double specificImpulse() const;
    double flameTemperature() const;
    double expansionFactor() const;
    double molecularWeight() const;
    double combustrionChamberCharacteristicLength() const;

signals:

public slots:

protected:
    Fuel m_fuel;
    Oxydizer m_oxydizer;
    double m_pressure;

    double m_mixtureRatio;
    double m_specificImpulse;
    double m_flameTemperature;
    double m_expansionFactor;
    double m_molecularWeight;
    double m_combustrionChamberCharacteristicLength;
};

#endif // FUELPAIR_H
