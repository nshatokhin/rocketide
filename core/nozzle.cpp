#include "nozzle.h"

#include <cmath>

#include <QDebug>

#include "constants.h"
#include "engine.h"
#include "fuelpair.h"

Nozzle::Nozzle(FuelPair *fuelPair, const double &mixtureConsumption, QObject *parent) :
    QObject(parent),
    m_fuelPair(fuelPair)
{
    m_temperatureAtNozzleThroat = m_fuelPair->flameTemperature() * (1 / (1 + (m_fuelPair->expansionFactor() - 1) / 2));
    m_pressureAtNozzleThroat = m_fuelPair->pressure() *
                pow(
                    1 + (m_fuelPair->expansionFactor() - 1) / 2,
                    -m_fuelPair->expansionFactor() / (m_fuelPair->expansionFactor() - 1)
                );

    qDebug() << "Temperature at nozzle throat:" << m_temperatureAtNozzleThroat;
    qDebug() << "Pressure at nozzle throat:" << m_pressureAtNozzleThroat;

    double specificGasConstant = Constants::gasConstant() / m_fuelPair->molecularWeight();
    m_nozzleThroatArea = (mixtureConsumption / m_pressureAtNozzleThroat) *
                            sqrt(
                                specificGasConstant * m_fuelPair->flameTemperature() /
                                (m_fuelPair->expansionFactor() * Constants::gravityConstant())
                            );

    qDebug() << "Nozzle throat area:" << m_nozzleThroatArea;

    m_gasSquareMachNumber = 2 / (m_fuelPair->expansionFactor() - 1) *
                      (
                          pow(m_fuelPair->pressure() / Constants::atmospherePressure(),
                             (m_fuelPair->expansionFactor() - 1) / m_fuelPair->expansionFactor()) - 1
                      );
    m_gasMachNumber = sqrt(m_gasSquareMachNumber);

    qDebug() << "Gas square mach number:" << m_gasSquareMachNumber;
    qDebug() << "Gas mach number:" << m_gasMachNumber;

    m_nozzleExitArea = m_nozzleThroatArea / m_gasMachNumber *
                       pow(
                           (1 + ((m_fuelPair->expansionFactor() - 1) / 2) * m_gasSquareMachNumber) / ((m_fuelPair->expansionFactor() + 1) / 2),
                           (m_fuelPair->expansionFactor() + 1) / (2 * (m_fuelPair->expansionFactor() - 1))
                       );

    qDebug() << "Nozzle exit area:" << m_nozzleExitArea;

    m_throatRadius = sqrt(m_nozzleThroatArea / M_PI);
    m_exitRadius = sqrt(m_nozzleExitArea / M_PI);

    qDebug() << "Nozzle throat radius:" << m_throatRadius;
    qDebug() << "Nozzle exit radius:" << m_exitRadius;

    m_convergenceAngle = 60;
    m_divergenceAngle = 15;
}

double Nozzle::throatArea() const
{
    return m_nozzleThroatArea;
}

double Nozzle::exitArea() const
{
    return m_nozzleExitArea;
}

double Nozzle::throatRadius() const
{
    return m_throatRadius;
}

double Nozzle::exitRadius() const
{
    return m_exitRadius;
}

double Nozzle::convergenceAngle() const
{
    return m_convergenceAngle;
}

double Nozzle::divergenceAngle() const
{
    return m_divergenceAngle;
}
