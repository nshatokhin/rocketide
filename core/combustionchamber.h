#ifndef COMBUSTIONCHAMBER_H
#define COMBUSTIONCHAMBER_H

#include <QObject>

class Nozzle;

class CombustionChamber : public QObject
{
    Q_OBJECT
public:
    explicit CombustionChamber(const double &characteristicLength, Nozzle * nozzle, QObject *parent = 0);

    double volume() const;

signals:

public slots:

protected:
    double m_characteristicLength;

    Nozzle * m_nozzle;

    double m_convergentVolumeRatio;

    double m_volume;
    double m_chamberRadius;
    double m_chamberArea;
    double m_chamberLength;

};

#endif // COMBUSTIONCHAMBER_H
