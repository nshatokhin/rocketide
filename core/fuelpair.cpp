#include "fuelpair.h"

FuelPair::FuelPair(const FuelPair::Fuel &fuel, const FuelPair::Oxydizer &oxydizer, const double &pressure, QObject *parent) :
    QObject(parent),
    m_fuel(fuel),
    m_oxydizer(oxydizer),
    m_pressure(pressure)
{
    m_mixtureRatio = 2.5;
    m_specificImpulse = 261;
    //m_flameTemperature = 3445.372;
    m_flameTemperature = 6202;
    m_expansionFactor = 1.2;
    m_molecularWeight = 24;
    m_combustrionChamberCharacteristicLength = 60;
}

FuelPair::FuelPair(const FuelPair &obj) :
    QObject(obj.parent()),
    m_fuel(obj.m_fuel),
    m_oxydizer(obj.m_oxydizer),
    m_pressure(obj.m_pressure),
    m_mixtureRatio(obj.m_mixtureRatio),
    m_specificImpulse(obj.m_specificImpulse),
    m_flameTemperature(obj.m_flameTemperature),
    m_expansionFactor(obj.m_expansionFactor),
    m_molecularWeight(obj.m_molecularWeight),
    m_combustrionChamberCharacteristicLength(obj.m_combustrionChamberCharacteristicLength)
{

}

double FuelPair::pressure() const
{
    return m_pressure;
}

double FuelPair::expansionFactor() const
{
    return m_expansionFactor;
}

double FuelPair::molecularWeight() const
{
    return m_molecularWeight;
}

double FuelPair::combustrionChamberCharacteristicLength() const
{
    return m_combustrionChamberCharacteristicLength;
}

double FuelPair::specificImpulse() const
{
    return m_specificImpulse;
}

double FuelPair::flameTemperature() const
{
    return m_flameTemperature;
}

double FuelPair::mixtureRatio() const
{
    return m_mixtureRatio;
}
