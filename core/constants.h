#ifndef CONSTANTS_H
#define CONSTANTS_H

#include <QObject>

class Constants
{
public:
    Constants();

    static double gasConstant();
    static double gravityConstant();

    static double atmospherePressure();
};

#endif // CONSTANTS_H
