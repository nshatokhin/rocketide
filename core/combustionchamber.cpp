#include "combustionchamber.h"

#include <cmath>

#include <QDebug>

#include "nozzle.h"

CombustionChamber::CombustionChamber(const double &characteristicLength,
                                     Nozzle * nozzle,
                                     QObject *parent) :
    QObject(parent),
    m_characteristicLength(characteristicLength),
    m_nozzle(nozzle)
{
    m_convergentVolumeRatio = 0.1;

    m_volume = m_characteristicLength * m_nozzle->throatArea();
    m_chamberRadius = 5 * m_nozzle->throatRadius();
    m_chamberArea = M_PI * pow(m_chamberRadius, 2);
    m_chamberLength = m_volume / ((1 + m_convergentVolumeRatio) * m_chamberArea);

    qDebug() << "Combustion chamber volume:" << m_volume << "m^3";
    qDebug() << "Combustion chamber radius:" << m_chamberRadius;
    qDebug() << "Combustion chamber area:" << m_chamberArea;
    qDebug() << "Combustion chamber length:" << m_chamberLength;
}

double CombustionChamber::volume() const
{
    return m_volume;
}
