#include "engine.h"

#include <QDebug>

#include "combustionchamber.h"
#include "nozzle.h"

Engine::Engine(const double &thrust, FuelPair *fuelPair, QObject *parent) :
    QObject(parent),
    m_thrust(thrust),
    m_fuelPair(fuelPair)
{
    m_mixtureConsumpsion = m_thrust / m_fuelPair->specificImpulse();
    m_fuelConsumption = m_mixtureConsumpsion / (m_fuelPair->mixtureRatio() + 1);
    m_oxydizerConsumption = m_mixtureConsumpsion - m_fuelConsumption;

    qDebug() << "Mixture consumption:" << m_mixtureConsumpsion << "kg/s";
    qDebug() << "Fuel consumption:" << m_fuelConsumption << "kg/s";
    qDebug() << "Oxydizer consumption:" << m_oxydizerConsumption << "kg/s";

    m_nozzle = new Nozzle(m_fuelPair, m_mixtureConsumpsion, this);
    m_combustionChamber = new CombustionChamber(m_fuelPair->combustrionChamberCharacteristicLength(),
                                                m_nozzle, this);
}

double Engine::mixtureConsumption() const
{
    return m_mixtureConsumpsion;
}
