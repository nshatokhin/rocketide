#include "constants.h"

Constants::Constants()
{

}

double Constants::gasConstant()
{
    //return 8.314459848;
    return 1545.32;
}

double Constants::gravityConstant()
{
    //return 9.8;
    return 32.2;
}

double Constants::atmospherePressure()
{
    //return 101325;
    return 14.7;
}
