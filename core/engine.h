#ifndef ENGINE_H
#define ENGINE_H

#include <QObject>

#include "fuelpair.h"
#include "nozzle.h"

class CombustionChamber;
class FuelPair;

class Engine : public QObject
{
    Q_OBJECT
public:
    explicit Engine(const double &thrust, FuelPair *fuelPair, QObject *parent = 0);

    double mixtureConsumption() const;
signals:

public slots:

protected:
    double m_thrust;

    FuelPair * m_fuelPair;

    CombustionChamber * m_combustionChamber;
    Nozzle * m_nozzle;

    double m_mixtureConsumpsion;
    double m_fuelConsumption;
    double m_oxydizerConsumption;
};

#endif // ENGINE_H
