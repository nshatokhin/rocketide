#include "mainarea.h"
#include "ui_mainarea.h"

#include <QDebug>
#include <QMdiSubWindow>

MainArea::MainArea(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainArea)
{
    ui->setupUi(this);

    connectActions();
}

MainArea::~MainArea()
{
    disconnectActions();

    delete ui;
}

void MainArea::createWindow()
{
    QWidget * window = new QWidget();
    window->setAttribute(Qt::WA_DeleteOnClose);

    QMdiSubWindow *subWindow = ui->mdiArea->addSubWindow(window);

    if(subWindow) {
        subWindow->setGeometry(0, 0, 200, 100);
        subWindow->show();
    }
}

bool MainArea::connectActions()
{
    connect(ui->actionNewProject, SIGNAL(triggered(bool)), this, SLOT(createWindow()));

    return true;
}

bool MainArea::disconnectActions()
{
    disconnect(ui->actionNewProject, SIGNAL(triggered(bool)), this, SLOT(createWindow()));

    return true;
}
