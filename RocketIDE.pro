#-------------------------------------------------
#
# Project created by QtCreator 2016-11-06T13:14:55
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = RocketIDE
TEMPLATE = app


SOURCES += main.cpp\
        mainarea.cpp \
    core/engine.cpp \
    core/constants.cpp \
    core/fuelpair.cpp \
    core/nozzle.cpp \
    core/combustionchamber.cpp

HEADERS  += mainarea.h \
    core/engine.h \
    core/constants.h \
    core/fuelpair.h \
    core/nozzle.h \
    core/combustionchamber.h

FORMS    += mainarea.ui
